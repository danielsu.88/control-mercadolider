import * as types from './mutation-types'


export const fetchVentas = ({ commit, dispatch, state }, params) => {
    return new Promise((resolve, reject) => {
        console.log(params)
        window.axios
            .get(`/api/v1/ventas`, { params })
            .then((response) => {
                console.log('bien bien', response)
                commit(types.SET_VENTAS_REPORTS_DATA, response.data)
                resolve(response)
            })
            .catch((err) => {
                reject(err.response)
            })
    })
}
export const fetchVenta = ({ commit, dispatch }, id) => {
    return new Promise((resolve, reject) => {
        window.axios
            .get(`/api/v1/ventas/${id}`)
            .then((response) => {
                console.log('bien', response)
                resolve(response)
            })
            .catch((err) => {
                reject(err)
            })
    })
}
export const UpdateVentas = ({ commit, dispatch, state }, id) => {
    return new Promise((resolve, reject) => {
        console.log('aquiiiiiiiiiiiiiiii', id)
        window.axios
            .put(`/api/v1/ventas/edit/`, { codigo: id })
            .then((response) => {
                console.log('Primera Respuesta', response)
                if (response.data.success) {
                    console.log('Respuesta', response)

                    //commit(response.data)
                }
                resolve(response)
            })
            .catch((err) => {
                reject(err.response)
            })
    })
}
export const updateVenta = ({ commit, dispatch, state }, data) => {
        return new Promise((resolve, reject) => {
            window.axios
                .put(`/api/v1/ventas/update/`, data)
                .then((response) => {
                    console.log(response)
                    if (response.data.success) {
                        //commit(response.data)
                    }
                    resolve(response)
                })
                .catch((err) => {
                    reject(err)
                })
        })
    }
    /*export const getSalesItemsReports = ({ commit, dispatch, state }, params) => {
      return new Promise((resolve, reject) => {
        window.axios
          .get(`/reports/sales/items/list/${params.unique_hash}`, { params })
          .then((response) => {
            commit(types.SET_SALES_REPORTS_DATA, response.data)
            resolve(response)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }

    export const getProfitLossReport = ({ commit, dispatch, state }, params) => {
      return new Promise((resolve, reject) => {
        window.axios
          .get(`/reports/profit-loss/list/${params.unique_hash}`, { params })
          .then((response) => {
            resolve(response)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }

    export const getExpensesReport = ({ commit, dispatch, state }, params) => {
      return new Promise((resolve, reject) => {
        window.axios
          .get(`/reports/expenses/list/${params.unique_hash}`, { params })
          .then((response) => {
            resolve(response)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }

    export const getTaxReport = ({ commit, dispatch, state }, params) => {
      return new Promise((resolve, reject) => {
        window.axios
          .get(`/reports/tax-summary/list/${params.unique_hash}`, { params })
          .then((response) => {
            resolve(response)
          })
          .catch((err) => {
            reject(err)
          })
      })
    }*/