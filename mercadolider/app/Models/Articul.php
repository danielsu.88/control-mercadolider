<?php

namespace Kanakku\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articul extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id',
        'codigo',
        'nombre',
        'marca',
        'status',
        'cantidad',
        'stockact',
        'observa'
    ];
    
    public function scopeApplyFilters($query, array $filters)
    {
        $filters = collect($filters);

        if ($filters->get('search')) {
            $query->whereSearch($filters->get('search'));
        }

        if ($filters->get('codigo')) {
            $query->whereCodigo($filters->get('codigo'));
        }

        if ($filters->get('nombre')) {
            $query->whereNombre($filters->get('nombre'));
        }

        if ($filters->get('marca')) {
            $query->whereMarca($filters->get('marca'));
        }
        if ($filters->get('status')) {
            $query->whereStatus($filters->get('status'));
        }

        if ($filters->get('orderByField') || $filters->get('orderBy')) {
            $field = $filters->get('orderByField') ? $filters->get('orderByField') : 'codigo';
            $orderBy = $filters->get('orderBy') ? $filters->get('orderBy') : 'asc';
            $query->whereOrder($field, $orderBy);
        }
    }
    public function scopeWhereOrder($query, $orderByField, $orderBy)
    {
        $query->orderBy($orderByField, $orderBy);
    }

    public function scopeWhereSearch($query, $search)
    {
        foreach (explode(' ', $search) as $term) {
            $query->where(function ($query) use ($term) {
                $query->where('codigo', 'LIKE', '%' . $term . '%')
                    ->orWhere('nombre', 'LIKE', '%' . $term . '%')
                    ->orWhere('marca', 'LIKE', '%' . $term . '%')
                    ->orWhere('status', 'LIKE', '%' . $term . '%');
            });
        }
    }
    public function scopeWhereCodigo($query, $codigo)
    {
        return $query->where('codigo', 'LIKE', '%' . $codigo . '%');
    }

    public function scopeWhereNombre($query, $nombre)
    {
        return $query->where('nombre', 'LIKE', '%' . $nombre . '%');
    }

    public function scopeWhereMarca($query, $marca)
    {
        return $query->where('marca', 'LIKE', '%' . $marca . '%');
    }
    public function scopeWhereStatus($query, $status)
    {
        return $query->where('status', 'LIKE', '%' . $status . '%');
    }

}
