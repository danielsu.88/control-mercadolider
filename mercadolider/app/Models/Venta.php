<?php

namespace Kanakku\Models;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
//use Illuminate\Foundation\Auth\Venta as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Spatie\MediaLibrary\HasMedia;
//use Spatie\MediaLibrary\InteractsWithMedia;

class Venta extends Model 
{
      //use HasApiTokens, Notifiable,  HasCustomFieldsTrait;
        use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $fillable = [
        'codigo',
        'nombre',
        'marca',
        'stockact',
        'cantidad',
        'observa',
        'precio1'
    ];

   /* protected $hidden = [
        'password',
        'remember_token',
    ];*/

    /*protected $with = [
        'currency'
    ];

    protected $appends = [
        'formattedCreatedAt',
        'avatar'
    ];*/
   

}
