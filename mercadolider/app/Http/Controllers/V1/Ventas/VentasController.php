<?php

namespace Kanakku\Http\Controllers\V1\Ventas;

use Kanakku\Http\Controllers\Controller;
use Kanakku\Http\Requests\ArticulRequest;
use Kanakku\Http\Requests\VentaRequest;
use Kanakku\Models\Articul;
use Kanakku\Models\Artalle;
use Kanakku\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if($request->pages=="limit"){
            $limit = 100000;
            $venta = Articul::get();
        }else{
            $venta=[];
            $limit = $request->has('limit') ? $request->limit : 10;
        }
        if(Auth::user()->roles=='Vendedor'){
            $venta = Articul::get();

            $status='Orden Creada';
            $ventas = Order::join('articuls','articuls.id','=','orders.product_id')
            ->join('arttalles', 'articuls.codigo', '=', 'arttalles.codigo')
            ->where('orders.status',$status)
            ->applyFilters(
                $request->only([
                    'codigo',
                    'nombre',
                    'marca',
                    'status'
                    
                ])
            )
            ->latest()
            ->paginate($limit);

        }else if(Auth::user()->roles=='Cajero'){
            $venta=[];
            $status='Orden Creada';
            $ventas = Order::join('articuls','articuls.id','=','orders.product_id')
            ->join('arttalles', 'articuls.codigo', '=', 'arttalles.codigo')
            ->where('orders.status',$status)
            ->applyFilters(
                $request->only([
                    'codigo',
                    'nombre',
                    'marca',
                    'status'
                    
                ])
            )->latest()
            ->paginate($limit);

        }else if(Auth::user()->roles=='Expedicion'){
            $venta=[];
            $status='Facturado';
            $status1='Entregado';
            $ventas = Order::join('articuls','articuls.id','=','orders.product_id')
            ->join('arttalles', 'articuls.codigo', '=', 'arttalles.codigo')
            ->where('orders.status',$status)
            ->orWhere('orders.status',$status1)
            ->applyFilters(
                $request->only([
                    'codigo',
                    'nombre',
                    'marca',
                    'status'
                    
                ])
            )->latest()
            ->paginate($limit);

        }else{
            $venta = Articul::get();
            $ventas = Order::join('articuls','articuls.id','=','orders.product_id')
            ->join('arttalles', 'articuls.codigo', '=', 'arttalles.codigo')
            ->applyFilters(
                $request->only([
                    'codigo',
                    'nombre',
                    'marca',
                    'status'
                    
                ])
            )->latest()
            ->paginate($limit);
        }
        
        return response()->json([
            'ventas' => $ventas,
            'venta'=>$venta
        ]);
    }
     /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request){
        //return $request;
        if(Auth::user()->roles=='Vendedor'){
            foreach($request->value as $value){
                $status='Orden Creada';
                $data=[
                    'cliente'=>$request->cliente,
                    'product_id'=>$value->id,
                    'marca'=>$request->marca,
                    'price'=>$request->precio1,
                    'observacion'=>$request->observa,
                    'status'=>$status,
                    'user_id'=>Auth::user()->id
                ];
                $articuls=Order::create($data);
            }
        }else if(Auth::user()->roles=='Cajero'){
            $status='Facturado';
            $articuls=Order::where('product_id',$request->codigo)->update(['status'=>$status]);

        }else if(Auth::user()->roles=='Expedicion'){
            $status='Entregado';
            $articuls=Order::where('product_id',$request->codigo)->update(['status'=>$status]);

        }/*else{
            $articuls=Order::find($request->codigo);
            if($articuls->status=='Orden Creada'){
                $articuls->status='Facturado';
                $articuls->save();
            }else if($articuls->status=='Facturado'){
                $articuls->status='Entregado';
                $articuls->save();

            }else if($articuls->status=='Sin Ordenes'){
                $articuls->status='Orden Creada';
                $articuls->save();
            }
        }*/
    
        return response()->json([
            'venta' => $articuls,
            'success' => true
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ArticulRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArticulRequest $request)
    {
        $data = $request->validated();

        $venta = Articul::create($data);


        return response()->json([
            'venta' => $venta,
            'success' => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $articuls=Articul::join('arttalles', 'articuls.codigo', '=', 'arttalles.codigo')
        ->where('id',$id)->first();
        return response()->json([
            'articul' => $articuls,
            'success' => true
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        if(isset($request->precio1)){
            $articul=Articul::where('id',$request->id)->update(['precio1'=>$request->precio1,'observa'=>$request->observa]);
        }else{
            foreach($request->value as $value){
                $data=[
                    'cliente'=>$request->cliente,
                    //'observacion'=>$request->observa,
                    //'price'=>$request->precio1,
                    'status'=>'Orden Creada',
                    'product_id'=>$value['id'],
                    'usuario_id'=>Auth::user()->id,
                    //'sku'=>$request->marca
                ];
                $articul=Order::create($data);
            }
        }
        return response()->json([
            'venta' => $articul,
            'success' => true
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request)
    {
        if ($request->ventas) {
            Venta::destroy($request->ventas);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
